package org.example;

import org.example.service.DistancePredictionService;

public class Main {
  public static void main(String[] args) {
    final DistancePredictionService service = new DistancePredictionService();
    final int predict = service.predict(10, 10);
    System.out.println(predict);
  }
}
